package engine;

import java.io.IOException;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import data.Data;

/**
 * 
 * A class to extend Media Player and implement handling interfaces. I also
 * started implementing the ability to handle the sound changes due to incoming
 * notification sounds like phone or message alerts *
 * 
 * See android media player documentation for state graph this was built from.
 * 
 * http://developer.android.com/reference/android/media/MediaPlayer.html
 * 
 * @author Rick
 * 
 */
public class MyMediaPlayer implements MediaPlayer.OnPreparedListener,
		MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener,
		AudioManager.OnAudioFocusChangeListener {

	CommonVariables commonVariables = CommonVariables.getInstance();
	CommonWork commonWork = CommonWork.getInstance();

	public MediaPlayer mediaPlayer;
	Uri path = Uri.parse(Data.PATH + Data.TRACK_01);
	AudioManager am;
	int result;

	/**
	 * Used in testing to tell if the headphones were unplugged or not
	 */
	public boolean volumeSet = false;

	/**
	 * Used in testing to tell if the volume was changed.
	 */
	public float currentVolume = 0f;

	public enum State {
		Idle, Initialized, Prepared, Started, Preparing, Stopped, Paused, End, Error, PlaybackCompleted
	}

	public State currentState;

	public void init() {
		am = (AudioManager) commonVariables.context
				.getSystemService(Context.AUDIO_SERVICE);
		result = am.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
				AudioManager.AUDIOFOCUS_GAIN);
		mediaPlayer = new MediaPlayer();
		mediaPlayer.reset();
		mediaPlayer.setOnPreparedListener(this);
		mediaPlayer.setOnErrorListener(this);
		mediaPlayer.setOnCompletionListener(this);
		currentState = State.Idle;
	}

	public void start() {
		if (commonVariables.playMusic) {
			if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
				if (currentState != State.Idle)
					init();
				if (currentState != State.Preparing) {
					try {
						mediaPlayer
								.setDataSource(commonVariables.context, path);
						currentState = State.Initialized;
						mediaPlayer
								.setAudioStreamType(AudioManager.STREAM_MUSIC);
						mediaPlayer.setVolume(commonVariables.volume,
								commonVariables.volume);
						currentVolume = commonVariables.volume;
						mediaPlayer.prepareAsync();
						currentState = State.Preparing;
					} catch (IllegalArgumentException e) {
					} catch (SecurityException e) {
					} catch (IllegalStateException e) {
					} catch (IOException e) {
					}
				}
			}
		}
	}

	@Override
	public void onPrepared(MediaPlayer player) {
		// check for option to play music and resume last position
		if (currentState == State.Preparing) {
			currentState = State.Prepared;
			if (commonVariables.playMusic) {
				if (commonVariables.currentSoundPosition > 0) {
					mediaPlayer.seekTo(commonVariables.currentSoundPosition);
				}
				if (currentState != State.End && !player.isPlaying()) {
					player.start();
					currentState = State.Started;
				}
			}
		}
	}

	@Override
	public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
		currentState = State.Error;
		start();
		return true;
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		// Handle audio lowering and raising for other phone sounds
		switch (focusChange) {
		case AudioManager.AUDIOFOCUS_GAIN:
			// resume play back
			if (mediaPlayer == null)
				init();
			else if (!mediaPlayer.isPlaying()) {
				start();
			} else {
				mediaPlayer.setVolume(commonVariables.volume,
						commonVariables.volume);
			}
			break;
		case AudioManager.AUDIOFOCUS_LOSS:
			// lost focus for an unbounded amount of time. stop and release
			pause();
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			// lost focus for a short time, but we have to stop play back.
			if (mediaPlayer != null && mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				currentState = State.Paused;
				commonVariables.currentSoundPosition = mediaPlayer
						.getCurrentPosition();
			}
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
			if (mediaPlayer != null) {
				setNewVolume(0.1f);
			}
			break;
		}
	}

	public void resume() {
		// media player should have been destroyed in last pause
		init();
		start();
	}

	public void abandonFocus() {
		if (am != null) {
			am.abandonAudioFocus(this);
		}
	}

	public void pause() {
		abandonFocus();
		if (currentState != State.End && mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				currentState = State.Paused;
			}
			if (currentState == State.Started || currentState == State.Paused) {
				mediaPlayer.stop();
				currentState = State.Stopped;
			}
			mediaPlayer.release();
			currentState = State.End;
			mediaPlayer = null;
		}
	}

	public void setNewVolume(Float setVolume) {
		if (currentState != State.End && mediaPlayer.isPlaying()) {
			mediaPlayer.setVolume(setVolume, setVolume);
			currentVolume = setVolume;
			volumeSet = true;
		}
	}

	public void toggleMusic() {
		if (commonVariables.playMusic) {
			commonVariables.playMusic = false;
			commonWork.showToast(commonVariables.context, "Music off");
			pause();
		} else {
			commonVariables.playMusic = true;
			commonWork.showToast(commonVariables.context, "Music on");
			resume();
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		currentState = State.PlaybackCompleted;
		commonVariables.currentSoundPosition = 0;
		start();
	}

	public void onStop() {
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				currentState = State.Paused;
				commonVariables.currentSoundPosition = mediaPlayer
						.getCurrentPosition();
			}
		}
	}

	public void destroy() {
		// a final check when the app closes down for good
		if (mediaPlayer != null) {
			mediaPlayer.release();
			currentState = State.End;
			mediaPlayer = null;
		}
	}
}