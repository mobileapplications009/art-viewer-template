package engine;

import android.media.SoundPool;
import art.viewer.template.R;

/**
 * 
 * Sound Pool extension includes loading sound and playing sounds.
 * 
 * @author Rick
 *
 */
public class MySoundPool extends SoundPool {

	CommonVariables vars = CommonVariables.getInstance();
	CommonWork work = CommonWork.getInstance();

	@SuppressWarnings("deprecation")
	public MySoundPool(int maxStreams, int streamType, int srcQuality) {
		super(maxStreams, streamType, srcQuality);
		// create a new sound pool and set up sounds
		this.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			public void onLoadComplete(SoundPool soundPool, int sampleId,
					int status) {
				if (sampleId == vars.saveSound)
					vars.saveSoundLoaded = true;
				else if (sampleId == vars.pageTurnSound)
					vars.pageTurnLoaded = true;
			}
		});

		vars.saveSound = load(vars.context, R.raw.imagesaved, 1);
		vars.pageTurnSound = load(vars.context, R.raw.pageturn, 1);
	}

	/**
	 * Play the save sound if loaded and allowed by user
	 */
	public void playSaveSound() {
		// check for sound file to be loaded and wanting to be player
		if (vars.saveSoundLoaded && vars.playSaveSound) {
			play(vars.saveSound, vars.volume, vars.volume, 1, 0, 1f);
		}
	}

	/**
	 * Play the page turn sound if loaded and allowed by user
	 */
	public void playPageTurnSound() {
		// check for tap sound to be loaded and it in preferences
		if (vars.pageTurnLoaded && vars.playPageTurnSound) {
			play(vars.pageTurnSound, vars.volume, vars.volume, 1, 0, 1f);
		}
	}

	/**
	 * Checks for the sounds to be loaded before setting it false or true and
	 * sends a toast message to the user
	 */
	public void toggleSounds() {
		// assume turning sound off
		boolean soundOff = false;

		// check for sound loaded the change flag
		if (vars.saveSoundLoaded) {
			if (vars.playSaveSound) {
				vars.playSaveSound = false;
				soundOff = true;
			}
		}

		// again check for sound to be loaded first
		if (vars.pageTurnLoaded) {
			if (vars.playPageTurnSound) {
				vars.playPageTurnSound = false;
				soundOff = true;
			}
		}

		// if either sound had to be turned off then skip here
		if (!soundOff) {
			if (vars.saveSoundLoaded)
				vars.playSaveSound = true;
			if (vars.pageTurnLoaded)
				vars.playPageTurnSound = true;
			work.showToast(vars.context, "Sounds On");
		} else {
			work.showToast(vars.context, "Sounds Off");
		}

	}
}