package engine;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import art.viewer.template.R;

/**
 * A class to define when and what animations to use for views.
 * 
 * @author Rick
 *
 */
public class InOutAnimationSet {
	// contains a set of animations
	public Animation outAnimation;
	public Animation inAnimation;
	CommonVariables vars = CommonVariables.getInstance();

	public final int ANIMATION_COUNT = 3;

	public final static int EXPAND_COLLAPSE = 0;
	public final static int WARP_VERTICAL = 1;
	public final static int COLLAPSE_ROTATE_CENTER = 2;

	/**
	 * Page turn animation for right turn
	 * 
	 * @param anim
	 * @return
	 */
	public Animation getOutAnimationRight(int anim) {
		switch (anim) {
		case WARP_VERTICAL:
			outAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.warp_vertical);
			break;
		case COLLAPSE_ROTATE_CENTER:
			outAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.collapse_stack_right);
			break;
		case EXPAND_COLLAPSE:
			outAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.collapse_rotate_right);
			break;
		default:
			outAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.warp_vertical);
			break;
		}
		return outAnimation;
	}

	/**
	 * Page turn animation for left turn out
	 * 
	 * @param anim
	 * @return
	 */
	public Animation getOutAnimationLeft(int anim) {
		switch (anim) {
		case WARP_VERTICAL:
			outAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.warp_vertical);
			break;
		case COLLAPSE_ROTATE_CENTER:
			outAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.collapse_stack_left);
			break;
		case EXPAND_COLLAPSE:
			outAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.collapse_rotate_left);
			break;
		default:
			outAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.warp_vertical);
			break;
		}
		return outAnimation;
	}

	/**
	 * Animation for the new image to show up from the right click
	 * 
	 * @param anim
	 * @return
	 */
	public Animation getInAnimationRight(int anim) {
		switch (anim) {
		case WARP_VERTICAL:
			inAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.fade_in);
			break;
		case COLLAPSE_ROTATE_CENTER:
			inAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.top_right_corner_rotate_in);
			break;
		case EXPAND_COLLAPSE:
			inAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.expand);
			break;
		default:
			inAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.fade_in);
			break;
		}
		return inAnimation;
	}

	/**
	 * Animation for the left side to show up.
	 * 
	 * @param anim
	 * @return
	 */
	public Animation getInAnimationLeft(int anim) {
		switch (anim) {
		case WARP_VERTICAL:
			inAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.fade_in);
			break;
		case COLLAPSE_ROTATE_CENTER:
			inAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.top_left_corner_rotate_in);
			break;
		case EXPAND_COLLAPSE:
			inAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.expand);
			break;
		default:
			inAnimation = AnimationUtils.loadAnimation(vars.context,
					R.anim.fade_in);
			break;
		}
		return inAnimation;
	}
}