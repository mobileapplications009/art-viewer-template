package engine;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class CommonWork {

	private volatile static CommonWork instance;

	/**
	 * Private constructor prevents access from other classes without using the
	 * getInstance method.
	 */
	private CommonWork() {

	}

	public static CommonWork getInstance() {
		if (instance == null)
			synchronized (CommonWork.class) {
				if (instance == null)
					instance = new CommonWork();
			}
		return instance;
	}

	Toast toast;

	/**
	 * Keep only one toast visible at one time.
	 * 
	 * @param cont
	 * @param message
	 */
	public void showToast(Context cont, String message) {
		// create if not, or set text to it
		if (toast == null) {
			toast = Toast.makeText(cont, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
		}
		if (!toast.getView().isShown()) {
			toast.setText(message);
			toast.show();
		} else {
			toast.cancel();
			toast.setText(message);
			toast.show();
		}
	}
}
