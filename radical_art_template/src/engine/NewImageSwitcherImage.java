package engine;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ImageSwitcher;
import data.Data;

/**
 * Class to load a new image into the view, will replace a current loading
 * animation if needed on fast button pressing.
 * 
 * @author Rick
 *
 */
public class NewImageSwitcherImage extends
		AsyncTask<Object, Object, BitmapDrawable> {

	WeakReference<ImageSwitcher> is;
	CommonVariables common = CommonVariables.getInstance();

	/**
	 * The weak reference will be where the new image is set, then its put into
	 * view.
	 * 
	 * @param imageSwitcher
	 */
	public NewImageSwitcherImage(ImageSwitcher imageSwitcher) {
		is = new WeakReference<ImageSwitcher>(imageSwitcher);
	}

	/**
	 * work to be done off the UI thread to load bitmap
	 */
	@Override
	protected BitmapDrawable doInBackground(Object... params) {
		WindowManager wm = (WindowManager) common.context
				.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(metrics);

		common.screenH = metrics.heightPixels;
		common.screenW = metrics.widthPixels;

		Bitmap bitmap = decodeSampledBitmapFromResource(common.res,
				Data.PICS[common.currentImagePosition], common.screenW,
				common.screenH);
		bitmap = Bitmap.createScaledBitmap(bitmap, common.screenW,
				common.screenH, true);

		BitmapDrawable bd = new BitmapDrawable(common.res, bitmap);

		return bd;
	}

	/**
	 * Asynchronous work to be done before execution, none here.
	 */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	/**
	 * After loading bitmap set to the image switcher
	 */
	@Override
	protected void onPostExecute(BitmapDrawable result) {
		super.onPostExecute(result);
		if (is != null) {
			if (is.get() != null) {
				ImageSwitcher iss = is.get();
				if (iss != null)
					iss.setImageDrawable(result);
			}
		}
	}

	/**
	 * Android given method for finding bitmaps size per the user's phone
	 * 
	 * @param options use to find optimal bitmap size
	 * @param reqWidth screen width
	 * @param reqHeight screen height
	 * @return options attribute
	 */
	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee a final image with both dimensions larger than or equal
			// to the requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	/**
	 * Android given method to decode and return the bitmap
	 * 
	 * @param res application resources
	 * @param resId image to be decoded
	 * @param reqWidth screen width
	 * @param reqHeight screen height
	 * @return bitmap that can be scaled
	 */
	public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}
}
