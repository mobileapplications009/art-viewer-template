package engine;

import android.content.Context;
import android.content.res.Resources;

/**
 * 
 * Singleton class used access variables that could modified and accessed by
 * other classes.
 * 
 * @author Rick
 *
 */
public class CommonVariables {

	private volatile static CommonVariables instance;

	public Context context;

	/**
	 * Used to obtain the strings from strings.xml and used as parameter in
	 * methods that need project resources as a parameter.
	 */
	public Resources res;

	public float volume = 1.0f;

	public int currentSoundPosition;
	public int currentImagePosition;

	public int saveSound;
	public int pageTurnSound;

	public boolean playSaveSound = true;
	public boolean saveSoundLoaded;

	public boolean playChimeSound = true;
	public boolean chimeLoaded;

	public boolean playPageTurnSound = true;
	public boolean pageTurnLoaded;

	public boolean playMusic = true;

	public int screenH, screenW;

	public int pageTurnMode = 0;

	/**
	 * Singleton variables class
	 * 
	 * @return
	 */
	public static CommonVariables getInstance() {
		if (instance == null)
			synchronized (CommonVariables.class) {
				if (instance == null)
					instance = new CommonVariables();
			}
		return instance;
	}

	/**
	 * Private constructor ensures the getInstance method is the only access
	 * point.
	 */
	private CommonVariables() {

	}
}