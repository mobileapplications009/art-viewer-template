package engine;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher.ViewFactory;
import art.viewer.template.R;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import data.Data;

/**
 * 
 * A class that is the main application implementation.
 * 
 * @author Rick
 *
 */
public class ArtViewer {

	/**
	 * The imageSwitcher is used to show the images. The Animations are set to
	 * in and out variables.
	 */
	public ImageSwitcher imageSwitcher;

	/**
	 * Store the game state variables in shared preferences.
	 */
	SharedPreferences sharedpreferences;

	/**
	 * The audio manager for the user's sound, changing these is to use caution,
	 * user sound settings can be affected and effected.
	 */
	AudioManager audioManager;

	/**
	 * Common variables are used to share variables between classes.
	 */
	CommonVariables commonVariables = CommonVariables.getInstance();

	/**
	 * Current work shared between classes.
	 */
	CommonWork commonWork = CommonWork.getInstance();

	/**
	 * Media Player controller.
	 */
	public MyMediaPlayer myMediaPlayer = new MyMediaPlayer();

	/**
	 * Sound loading and playing.
	 */
	MySoundPool mySoundPool;

	/**
	 * Contains three animations that rotate on selection.
	 */
	InOutAnimationSet inOutSet = new InOutAnimationSet();

	/**
	 * Used to switch images cleanly and behind the scenes.
	 */
	AsyncTask<Object, Object, BitmapDrawable> imageSwitcherTask;

	/**
	 * The ad banner.
	 */
	private AdView adView;

	/**
	 * 
	 * Called to start the application by checking for previous state first and
	 * then recreating the rest.
	 * 
	 * @param context
	 */
	public void init(Context context) {
		commonVariables.context = context;
		commonVariables.res = context.getResources();

		getSharedPrefs();

		audioInit();

		imageSwitcherInit((Activity) context);

		adView = (AdView) ((Activity) context).findViewById(R.id.adView);
		adView.setBackgroundColor(Color.BLACK);
		final AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		adView.loadAd(adRequestBuilder.build());
	}

	private void imageSwitcherInit(final Activity act) {
		imageSwitcher = (ImageSwitcher) act
				.findViewById(R.id.mainImageSwitcher);
		imageSwitcher.setFactory(new ViewFactory() {
			@Override
			public View makeView() {
				ImageView myView = new ImageView(act.getApplicationContext());
				myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
				myView.setLayoutParams(new ImageSwitcher.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				return myView;
			}
		});

		imageSwitcher.setBackgroundResource(R.drawable.gradient_background);
		imageSwitcherTask = new NewImageSwitcherImage(imageSwitcher)
				.execute(new Object());
	}

	private void audioInit() {
		myMediaPlayer = new MyMediaPlayer();
		myMediaPlayer.init();

		mySoundPool = new MySoundPool(15, AudioManager.STREAM_MUSIC, 100);
	}

	private void getSharedPrefs() {
		sharedpreferences = commonVariables.context.getSharedPreferences(
				commonVariables.res.getString(R.string.my_preferences),
				Context.MODE_PRIVATE);

		if (sharedpreferences.contains(commonVariables.res
				.getString(R.string.current_image))) {
			commonVariables.currentImagePosition = sharedpreferences.getInt(
					commonVariables.res.getString(R.string.current_image), 0);
		}

		if (sharedpreferences.contains(commonVariables.res
				.getString(R.string.current_position))) {
			commonVariables.currentSoundPosition = sharedpreferences
					.getInt(commonVariables.res
							.getString(R.string.current_position), 0);
		}

		if (sharedpreferences.contains(commonVariables.res
				.getString(R.string.play_music))) {
			commonVariables.playMusic = sharedpreferences.getBoolean(
					commonVariables.res.getString(R.string.play_music), true);
		}

		if (sharedpreferences.contains(commonVariables.res
				.getString(R.string.turn_mode))) {
			commonVariables.pageTurnMode = sharedpreferences.getInt(
					commonVariables.res.getString(R.string.turn_mode), 0);
		}

		if (sharedpreferences.contains(commonVariables.res
				.getString(R.string.play_page_turn))) {
			commonVariables.playPageTurnSound = sharedpreferences.getBoolean(
					commonVariables.res.getString(R.string.play_page_turn),
					false);
		}

		if (sharedpreferences.contains(commonVariables.res
				.getString(R.string.play_save))) {
			commonVariables.playSaveSound = sharedpreferences.getBoolean(
					commonVariables.res.getString(R.string.play_save), false);
		}
	}

	/**
	 * Left page turn switches image and starts page turn animation left
	 * 
	 * @param v
	 */
	public void left(View view) {
		commonVariables.currentImagePosition--;
		if (commonVariables.currentImagePosition < 0)
			commonVariables.currentImagePosition = Data.PICS.length - 1;

		Animation in = inOutSet
				.getInAnimationLeft(commonVariables.pageTurnMode);
		Animation out = inOutSet
				.getOutAnimationLeft(commonVariables.pageTurnMode);

		imageSwitcher.setInAnimation(in);
		imageSwitcher.setOutAnimation(out);

		if (imageSwitcherTask != null)
			imageSwitcherTask.cancel(true);

		imageSwitcherTask = new NewImageSwitcherImage(imageSwitcher)
				.execute(new Object());

		mySoundPool.playPageTurnSound();
	}

	/**
	 * Right page turn switches image and starts page turn animation right
	 * 
	 * @param v
	 */
	public void right(View view) {
		commonVariables.currentImagePosition++;
		if (commonVariables.currentImagePosition > Data.PICS.length - 1)
			commonVariables.currentImagePosition = 0;

		Animation in = inOutSet
				.getInAnimationRight(commonVariables.pageTurnMode);
		Animation out = inOutSet
				.getOutAnimationRight(commonVariables.pageTurnMode);

		imageSwitcher.setInAnimation(in);
		imageSwitcher.setOutAnimation(out);

		if (imageSwitcherTask != null)
			imageSwitcherTask.cancel(true);

		imageSwitcherTask = new NewImageSwitcherImage(imageSwitcher)
				.execute(new Object());

		mySoundPool.playPageTurnSound();
	}

	/**
	 * Handler for the item selected from the menu
	 * 
	 * @param item
	 * @return
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_blog_devart:
			Intent intent = new Intent(Intent.ACTION_VIEW,
					Uri.parse(commonVariables.res
							.getString(R.string.deviant_link)));
			commonVariables.context.startActivity(intent);
			return true;
		case R.id.menu_blog_cure:
			Intent intent1 = new Intent(Intent.ACTION_VIEW,
					Uri.parse(commonVariables.res
							.getString(R.string.facebook_link)));
			commonVariables.context.startActivity(intent1);
			return true;
		case R.id.menu_blog_music:
			Intent intent2 = new Intent(Intent.ACTION_VIEW,
					Uri.parse(commonVariables.res
							.getString(R.string.music_link)));
			commonVariables.context.startActivity(intent2);
			return true;
		case R.id.menu_save_image:
			new SavePhoto(commonVariables.currentImagePosition);
			return true;
		case R.id.menu_music_toggle:
			toggleMusic();
			return true;
		case R.id.menu_sound_toggle:
			toggleSounds();
			return true;
		case R.id.menu_turn_toggle:
			togglePageTurns();
			return true;
		default:
			return true;
		}
	}

	/**
	 * Switch music playing on/off
	 */
	public void toggleMusic() {
		myMediaPlayer.toggleMusic();
	}

	/**
	 * Toggle sounds on/off
	 */
	public void toggleSounds() {
		mySoundPool.toggleSounds();
	}

	/**
	 * Stores the mode for page turn animations
	 */
	private void togglePageTurns() {
		if (commonVariables.pageTurnMode >= (inOutSet.ANIMATION_COUNT - 1))
			commonVariables.pageTurnMode = 0;
		else
			commonVariables.pageTurnMode++;

		commonWork.showToast(commonVariables.context, "Mode: 0"
				+ commonVariables.pageTurnMode);
	}

	/**
	 * On press of image hide UI and switch animations
	 * 
	 * @param view
	 */
	public void imageSwitcher(View view) {
		// hide or show full image if clicking on view and not a page turn
		Activity act = (Activity) commonVariables.context;
		ImageButton left = (ImageButton) act.findViewById(R.id.leftButton);
		ImageButton right = (ImageButton) act.findViewById(R.id.rightButton);

		if (right.getVisibility() == View.VISIBLE
				&& left.getVisibility() == View.VISIBLE) {
			right.setVisibility(View.INVISIBLE);
			left.setVisibility(View.INVISIBLE);
		} else {
			right.setVisibility(View.VISIBLE);
			left.setVisibility(View.VISIBLE);
			// as a bonus change the mode for the animations
			togglePageTurns();
		}
	}

	/**
	 * When headphones become unplugged or notifications on the phone alert the
	 * user the sound should quiet
	 */
	public void quietSound() {
		// set volume to low
		if (myMediaPlayer != null) {
			myMediaPlayer.setNewVolume(0.1f);
		}
	}

	/**
	 * Handle resuming the music and adview
	 */
	public void resume() {
		adView.resume();
		myMediaPlayer.resume();
	}

	/**
	 * Pause the adview and mediaplayer
	 */
	public void pause() {
		adView.pause();
		myMediaPlayer.pause();
	}

	/**
	 * Get current music position for replay at last spot and save variables to
	 * shared preferences.
	 */
	public void stop() {
		myMediaPlayer.onStop();

		Editor editor = sharedpreferences.edit();
		editor.putInt(commonVariables.res.getString(R.string.current_image),
				commonVariables.currentImagePosition);
		editor.putInt(commonVariables.res.getString(R.string.turn_mode),
				commonVariables.pageTurnMode);
		editor.putBoolean(commonVariables.res.getString(R.string.play_music),
				commonVariables.playMusic);
		editor.putBoolean(commonVariables.res.getString(R.string.play_save),
				commonVariables.playSaveSound);
		editor.putBoolean(
				commonVariables.res.getString(R.string.play_page_turn),
				commonVariables.playPageTurnSound);
		editor.putInt(commonVariables.res.getString(R.string.current_position),
				commonVariables.currentSoundPosition);
		editor.apply();
	}

	/**
	 * Clean up sound pool, media player and adView
	 */
	public void destroy() {
		if (mySoundPool != null) {
			mySoundPool.release();
			mySoundPool = null;
		}

		if (myMediaPlayer != null) {
			myMediaPlayer.destroy();
			myMediaPlayer = null;
		}

		adView.destroy();
	}
}