package art.viewer.template;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import engine.ArtViewer;
import engine.CommonVariables;

/**
 * 
 * Starting point for the application as stated in the AndroidManifest.
 * 
 * @author Richard A. Perez
 *
 */
public class MainActivity extends Activity {

	/**
	 * Create reference for jUnit testing.
	 */
	public Menu menu;

	/**
	 * Sets the start volume to level the music so it isn't too loud for the
	 * user.
	 */
	final float START_VOLUME = .8F;

	/**
	 * Variables used in the application across classes.
	 */
	public CommonVariables commonVariables = CommonVariables.getInstance();

	/**
	 * Contains components for application.
	 */
	public ArtViewer artViewer = new ArtViewer();

	/**
	 * Allow the sound to quiet if headphones become unplugged.
	 */
	public NoisyAudioStreamReceiver myNoisyAudioStreamReceiver;

	/**
	 * Allow the system to quiet if receiving the ACTION_AUDIO_BECOMING_NOISY
	 * intent.
	 * 
	 */
	public class NoisyAudioStreamReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent
					.getAction())) {
				artViewer.quietSound();
			}
		}
	}

	/**
	 * Set the filter to listen for headphones to become unplugged.
	 */
	private IntentFilter intentFilter = new IntentFilter(
			AudioManager.ACTION_AUDIO_BECOMING_NOISY);

	/**
	 * Start the listener
	 */
	private void startPlayback() {
		registerReceiver(myNoisyAudioStreamReceiver, intentFilter);
	}

	/**
	 * Stop the listener
	 */
	private void stopPlayback() {
		unregisterReceiver(myNoisyAudioStreamReceiver);
	}

	/**
	 * Start of creating the application and setting the view.
	 * 
	 * @param savedInstanceState
	 *            not used
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// validate the user's google play is updated
		Integer resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		if (resultCode == ConnectionResult.SUCCESS) {
			// Do what you want
		} else {
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					this, 0);
			if (dialog != null) {
				// This dialog will help the user update to the latest
				// GooglePlayServices
				dialog.show();
			}
		}
		// start up the view
		artViewer.init(this);
	}

	/**
	 * Create the menu from an xml file.
	 * 
	 * @param menu
	 *            the menu to create
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		this.menu = menu;
		return true;
	}

	/**
	 * Direct the handler for the items selected.
	 * 
	 * @param item
	 *            the item selected.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return artViewer.onOptionsItemSelected(item);
	}

	/**
	 * Handler for the image switcher click, switches animations
	 * 
	 * @param view
	 *            the image switcher view clicked
	 */
	public void imageSwitcherClick(View view) {
		artViewer.imageSwitcher(view);
	}

	/**
	 * Page turn left handler
	 * 
	 * @param view
	 *            the left button clicked
	 */
	public void LeftArrowClick(View view) {
		artViewer.left(view);
	}

	/**
	 * Page turn right handler
	 * 
	 * @param view
	 *            the right button clicked
	 */
	public void RightArrowClick(View view) {
		artViewer.right(view);
	}

	/**
	 * Activity on resume, start music and listener
	 */
	@Override
	protected void onResume() {
		super.onResume();

		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		float streamVolume = (float) audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);

		if ((float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) > 10.0f) {
			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0);
		}

		commonVariables.volume = streamVolume
				/ (float) audioManager
						.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

		if (commonVariables.volume > START_VOLUME)
			commonVariables.volume = START_VOLUME;

		myNoisyAudioStreamReceiver = new NoisyAudioStreamReceiver();
		startPlayback();

		if (commonVariables.playMusic) {
			artViewer.resume();
		} else {
			artViewer.myMediaPlayer.abandonFocus();
		}

		// set animation for whole view not just the starting image
		Animation anim = AnimationUtils.loadAnimation(commonVariables.context,
				R.anim.expand);
		anim.reset();

		// this is the layout defined in the main activity as a relative layout
		View relativeLayout = findViewById(R.id.relativeLayout);
		relativeLayout.clearAnimation();
		relativeLayout.startAnimation(anim);
	}

	/**
	 * Start pausing classes and stop headphone listening.
	 */
	@Override
	protected void onPause() {
		super.onPause();
		stopPlayback();

		artViewer.pause();
	}

	/**
	 * Save items to shared preferences in art viewer
	 */
	@Override
	protected void onStop() {
		super.onStop();
		artViewer.stop();
	}

	/**
	 * Destroy any unneeded resources.
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		artViewer.destroy();
	}
}