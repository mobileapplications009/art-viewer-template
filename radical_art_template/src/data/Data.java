package data;

import art.viewer.template.R;

/**
 * 
 * A class to hold static data used in various classes.
 * 
 * @author Rick
 *
 */

public class Data {
	/**
	 * when you add images you need to add them here
	 */
	public final static int[] PICS = { R.drawable.image0, R.drawable.image1,
			R.drawable.image2, R.drawable.image3 };

	/**
	 * the path will be dependent on the unique package name you give your
	 * application.
	 */
	public final static String PATH = "android.resource://art.viewer.template/";

	/**
	 * Music track for the background
	 */
	public final static int TRACK_01 = R.raw.track01;
}